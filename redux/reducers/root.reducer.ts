import { combineReducers } from 'redux'
import navbarReducer from '../slices/navbar.slice'

const rootReducer = combineReducers({
  navbar: navbarReducer
})

export default rootReducer
