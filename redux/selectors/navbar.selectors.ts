import { RootState } from './../store'
import { createSelector } from 'reselect'

export const navbarItemsSelector = (state: RootState) => {
  return state.navbar.items
}

export const authedNavbarItemsSelector = createSelector(
  navbarItemsSelector,
  items => items.filter(item => item.authed)
)

export const publicNavbarItemsSelector = createSelector(
  navbarItemsSelector,
  items => items.filter(item => !item.authed)
)
