import { configureStore } from '@reduxjs/toolkit'
import { createWrapper } from 'next-redux-wrapper'
import rootReducer from './reducers/root.reducer'

const store = configureStore({
  reducer: rootReducer
})

const makeStore = () => store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default createWrapper(makeStore, {
  debug: process.env.NODE_ENV !== 'production'
})
