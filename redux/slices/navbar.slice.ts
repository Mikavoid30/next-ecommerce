import { createSlice } from '@reduxjs/toolkit'
import { NavbarItem } from '../../components/Navbar/Navbar.types'
import cmsApi from '../../lib/cms-api'

interface NavbarState {
  items: NavbarItem[]
}

const initialState = { items: [] } as NavbarState

const navbarSlice = createSlice({
  name: 'navbar',
  initialState,
  reducers: {
    fetchNavbarItems (state) {
      const items = cmsApi.getLocalNavbarItemsData()
      state.items = items
    }
  }
})

export const { fetchNavbarItems } = navbarSlice.actions
export default navbarSlice.reducer
