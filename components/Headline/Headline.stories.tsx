import React from 'react'
import { Story, Meta } from '@storybook/react'

import { Headline } from './Headline'
import { HeadlineProps, E_HeadlineLevels } from './Headline.types'

export default {
  title: 'Components/Headline',
  component: Headline
} as Meta

const Template: Story<HeadlineProps> = args => <Headline {...args} />

export const Level1 = Template.bind({})
Level1.args = {
  level: E_HeadlineLevels.ONE
}

export const Level2 = Template.bind({})
Level2.args = {
  level: E_HeadlineLevels.TWO
}
