export type HeadlineProps = {
  level: E_HeadlineLevels
  children: JSX.Element | string | JSX.Element[]
}
export enum E_HeadlineLevels {
  ONE = 1,
  TWO = 2,
  THREE = 3,
  FOUR = 4,
  FIVE = 5,
  SIX = 6
}
