import React from 'react'
import classNames from 'classnames'
import { E_HeadlineLevels, HeadlineProps } from './Headline.types'

import _ from './Headline.module.scss'

export function Headline ({
  level = E_HeadlineLevels.ONE,
  children = 'Level ' + level
}: HeadlineProps): JSX.Element {
  const classes = classNames(_.Headline, { [_['Headline--L' + level]]: true })
  const Tag = `h${level}` as keyof JSX.IntrinsicElements
  return (
    <div className={classes}>
      <Tag>{children}</Tag>
    </div>
  )
}
