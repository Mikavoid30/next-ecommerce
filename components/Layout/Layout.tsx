import React from 'react'
import { LayoutProps } from './Layout.types'

import _ from './Layout.module.scss'
import NavbarConnected from '../../containers/Navbar/NavbarConnected'

function Layout ({ children }: LayoutProps): JSX.Element {
  return (
    <div className={_.Layout}>
      <NavbarConnected dark isLoggedIn={false} />
      {children}
    </div>
  )
}

export default Layout
