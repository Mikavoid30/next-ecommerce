export type NavbarProps = {
  dark?: boolean
  isLoggedIn?: boolean
  navbarItems?: NavbarItem[]
}

export type NavbarItem = {
  id?: number
  label: string
  link: string
  authed: boolean
}
