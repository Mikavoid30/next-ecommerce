import React from 'react'
import { Story, Meta } from '@storybook/react'

import Navbar from './Navbar'
import { NavbarItem, NavbarProps } from './Navbar.types'

export default {
  title: 'Components/Navbar',
  component: Navbar
} as Meta

const navbarItems: NavbarItem[] = [
  { id: 1, label: 'first authed', link: 'first', authed: true },
  { id: 2, label: 'second authed', link: 'first', authed: true }
]
const Template: Story<NavbarProps> = args => (
  <Navbar {...args} navbarItems={navbarItems}></Navbar>
)

export const Default = Template.bind({})
Default.args = {
  authed: true,
  dark: true
}
