import React from 'react'
import classNames from 'classnames'
import { NavbarItem, NavbarProps } from './Navbar.types'

import _ from './Navbar.module.scss'
import { NextRouter, useRouter } from 'next/router'
import Link from 'next/link'

function Navbar ({ dark = false, navbarItems = [] }: NavbarProps): JSX.Element {
  const [mobileNavOpen, setMobileNavOpen] = React.useState<boolean>(false)

  const handleBurgerButtonClick = () => setMobileNavOpen(!mobileNavOpen)
  const mapNavItems = (router: NextRouter) => (item: NavbarItem) => {
    // Set active class when current route path = menu item
    const classes = classNames(_.NavbarItem, {
      [_['NavbarItem--active']]: item.link === router?.pathname
    })
    return (
      <li key={item.id} className={classes}>
        <Link href={item.link}>
          <a>{item.label}</a>
        </Link>
      </li>
    )
  }
  return (
    <div
      className={classNames(_.Navbar, {
        [_['Navbar--dark']]: !!dark
      })}
    >
      <button
        className={classNames(_.BurgerButton, {
          [_['BurgerButton--mobile-is-open']]: mobileNavOpen
        })}
        onClick={handleBurgerButtonClick}
      >
        <span></span>
        <span></span>
        <span></span>
      </button>
      <nav
        className={classNames(_.NavigationLinksContainer, {
          [_['NavigationLinksContainer--mobile-is-open']]: !!mobileNavOpen
        })}
      >
        <ul onClick={handleBurgerButtonClick}>
          {navbarItems.map(mapNavItems(useRouter()))}
        </ul>
      </nav>
    </div>
  )
}

export default Navbar
