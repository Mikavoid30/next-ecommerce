import { ROUTES_PATHS } from './../constants'
import shortid from 'shortid'

const navbarItems = [
  { label: 'Register', link: ROUTES_PATHS.REGISTER, authed: false },
  { label: 'Login', link: ROUTES_PATHS.LOGIN, authed: false },
  { label: 'My orders', link: ROUTES_PATHS.USER_ACCOUNT, authed: true },
  {
    label: 'My account',
    link: ROUTES_PATHS.USER_ORDERS,
    authed: true
  },
  { label: 'Logout', link: ROUTES_PATHS.LOGOUT, authed: true }
].map(item => ({ ...item, id: shortid.generate() }))

export default navbarItems
