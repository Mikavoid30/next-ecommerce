import navbarItemsData from '../data/navbar.data'

export function getLocalNavbarItemsData () {
  // for now, we are using local data
  return navbarItemsData
}

export default {
  getLocalNavbarItemsData
}
