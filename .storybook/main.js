const path = require('path')

module.exports = {
  stories: [
    '../stories/**/*.stories.mdx',
    '../components/**/*.stories.mdx',
    '../stories/**/*.stories.@(js|jsx|ts|tsx)',
    '../components/**/*.stories.@(js|jsx|ts|tsx)'
  ],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials'],
  webpackFinal: async (config, { configType }) => {
    config.module.rules.push({
      test: /\.scss$/,
      use: [
        'style-loader',
        {
          loader: require.resolve('css-loader'),
          options: {
            modules: true
          }
        },
        'sass-loader'
      ],
      include: path.resolve(__dirname, '../')
    })

    return config
  }
}
