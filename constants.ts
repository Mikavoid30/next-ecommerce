export const ROUTES_PATHS = {
  LOGIN: '/auth/login',
  REGISTER: '/auth/register',
  LOGOUT: '/auth/logout',
  USER_ACCOUNT: '/user/account',
  USER_ORDERS: '/user/order'
}
