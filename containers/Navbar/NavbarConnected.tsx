import React from 'react'
import Navbar from '../../components/Navbar/Navbar'
import { NavbarItem, NavbarProps } from '../../components/Navbar/Navbar.types'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { fetchNavbarItems } from '../../redux/slices/navbar.slice'

import {
  authedNavbarItemsSelector,
  publicNavbarItemsSelector
} from '../../redux/selectors/navbar.selectors'

function NavbarConnected ({
  isLoggedIn = false,
  ...props
}: NavbarProps): JSX.Element {
  const dispatch = useAppDispatch()
  React.useEffect(() => {
    dispatch(fetchNavbarItems())
  }, [])
  // Get each link data
  const navbarItemsData = useAppSelector(
    isLoggedIn ? authedNavbarItemsSelector : publicNavbarItemsSelector
  ) as NavbarItem[]

  return <Navbar navbarItems={navbarItemsData} {...props} />
}

export default NavbarConnected
