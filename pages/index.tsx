import Head from 'next/head'
import { Headline } from '../components/Headline/Headline'

export const Home = (): JSX.Element => (
  <div className='container'>
    <Head>
      <title>Next Ecommerce :)</title>
      <link rel='icon' href='/favicon.ico' />
    </Head>

    <main>
      <Headline level={1}>Lol</Headline>
    </main>
  </div>
)

export default Home
